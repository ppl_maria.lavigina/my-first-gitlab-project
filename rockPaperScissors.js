
    console.log("hi");

    const getUserChoice = (userInput) => {
      userInput = userInput.toLowerCase();

      if (
        userInput === "rock" ||
        userInput === "paper" ||
        userInput === "scissors"
      ) {
        return userInput;
      } else {
        console.log("Error! please type: rock, paper or scissors.");
      }
    };

    // computer makes a choice:
    function getComputerChoice() {
      //const getComputerChoice = () => {
      const randomNumber = Math.floor(Math.random() * 3);

      switch (randomNumber) {
        case 0:
          return "rock";
          break;
        case 1:
          return "paper";
          break;
        case 2:
          return "scissors";
      }
    } //end getComputerChoice


    //determine a winner
    const determineWinner = (userChoice, computerChoice) => {  
      if (userChoice === computerChoice) {
        return "the game is tie";
      }

      //if this is not a tie
      if (userChoice === "rock") {
        if (computerChoice === "paper") {
          return "computer won";
        } else {
          return "You won!";
        }
      }

      if (userChoice === "paper") {
        if (computerChoice === "scissors") {
          return "sorry computer won";
        } else {
          return "congratulations, you won!";
        }
      }

      if (userChoice === "scissors") {
        if (computerChoice === "rock") {
          return "sorry computer won";
        } else {
          return "congratulations, you won!";
        }
      }
    };

userInput = prompt("Make your choice");
let userChoice = getUserChoice(userInput);
let computerChoice = getComputerChoice();

console.log("You chose: ", userChoice);
console.log("The computer chose: ", computerChoice); //shows to us the computer choice
console.log(determineWinner(userChoice, computerChoice));

